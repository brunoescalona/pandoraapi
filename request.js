'use strict'

var request = require('request');
var crypto = require('crypto');

var apiEndPoint = 'https://tuner.pandora.com/services/json/';

var partnerId;
var partnerAuthToken;
var syncTime;

module.exports = {
    checkLicensing: checkLicensing,
    partnerAuthentication: partnerAuthentication,
    loginPandora: loginPandora
}


function checkLicensing(callback) {

    var checkLicensingMethod = 'test.checkLicensing';

    request.post({
            url: apiEndPoint + '?method=' + checkLicensingMethod
        },
        function(err, httpResponse, body) {
            if (err) {
                return callback(err, null);
            }
            callback(err, JSON.parse(body));
        });
}

function partnerAuthentication(callback) {

    var partnerAuthenticationMethod = 'auth.partnerLogin';

    var authData = {
        username: "android",
        password: "AC7IBG09A3DTSYM4R41UJWL07VLN8JI7",
        deviceModel: "android-generic",
        version: "5"
    }
    request.post({
            url: apiEndPoint + '?method=' + partnerAuthenticationMethod,
            body: JSON.stringify(authData)
        },
        function(err, httpResponse, body) {
            if (err) {
                return callback(err, null);
            }

            var response = JSON.parse(body);

            if (response.stat === 'ok') {
                partnerId = response.result.partnerId;
                partnerAuthToken = response.result.partnerAuthToken;
                syncTime = response.result.syncTime;
            }

            callback(err, response);
        });
}

function loginPandora() {
    /*console.log(partnerId);
    console.log(partnerAuthToken);
    console.log(syncTime);*/

    var decipher = crypto.createDecipheriv('bf-ecb', 'AC7IBG09A3DTSYM4R41UJWL07VLN8JI7','');
    decipher.setAutoPadding(false);
    var data = decipher.update('6923e263a8c3ac690646146b50065f43', "hex", "hex");
    console.log(data);
    data += decipher.final("hex");
    console.log(data);//
    var time = parseInt(data, 16);
    console.log(time);
}
