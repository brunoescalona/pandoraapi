'use strict'

var express = require('express');
var pandora = require('./request.js')
var app = express();

app.listen(3000, function() {
    console.log('Listening on port 3000');
});

app.get('/', function(req, res) {
    res.send('Bienvenido a la aplicación Node para obtener información de la API de Pandora');
});

app.get('/check-licensing', function(req, res) {
    pandora.checkLicensing(function(err, body) {
        if (err) res.send(err);
        res.send(body);
    });
});

app.get('/partner-authentication', function(req, res) {
    pandora.partnerAuthentication(function(err, body) {
        if (err) res.send(err);
        res.send(body);
    });
});

app.get('/login', function(req, res) {
    pandora.loginPandora();
    res.send();
});
