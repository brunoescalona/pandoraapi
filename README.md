Ejemplo en Node de Pandora Api
======

## Prerequisitos

- Tener instalado nodejs
- Clonar el repositorio


## Run
- en el directorio del repositorio

  ```
  $ npm install
  ```

  ```
  $ node .
  ```

- [localhost:3000](http://localhost:3000) aplicación corriendo en local


## Notas del código

- [/check-licensing](http://localhost:3000/check-licensing) método para comprobar si es posible obtener información
de la API de pandora. Si el resultado es false es necesario un VPN o que la aplicación corra en un servidor
situado en E.E.U.U. Para más información acceder a la documentación no oficial de Pandora para este [método](https://6xq.net/pandora-apidoc/json/authentication/#check-licensing).

- [/partner-authentication](http://localhost:3000/partner-authentication) método para autentificarnos con el partner obteniendo un token y un synctime. Para más información acceder a la documentación no oficial de Pandora para este [método](https://6xq.net/pandora-apidoc/json/authentication/#partner-login).